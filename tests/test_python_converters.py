# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
# 
# test_python_converters.py 
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-`           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J`   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD, http://wide.io/
# ----------------------------------------------------------------------
# CONFIDENTIAL
# ----------------------------------------------------------------------
# THIS FILE IS NOT YET DISTRIBUTED AND SHALL NOT BE REPRODUCED UNLESS YOU
# HAVE RECEIVED A WRITTEN AND SIGNED AUTHORISATION TO DO SO.
# FAILURE TO DO SO MAY EXPOSE YOU TO PROSECUTION.
# ----------------------------------------------------------------------
# THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
# KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
# PARTICULAR PURPOSE.
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
# python test_converters
import os
import numpy

os.environ["DJANGO_SETTINGS_MODULE"] = "aiosciweb1.settings"

from superglue.utils import superglue_converter, wideio_typeof, wideio_typeref


@superglue_converter(
    _from=wideio_typeof(1.0),
    _to=wideio_typeof(1)
)
def ftoi(f): return int(f)


@superglue_converter(
    _from=wideio_typeof(""),
    _to=wideio_typeof(1)
)
def atoi(i): return int(i)


@superglue_converter(
    _from=wideio_typeof(""),
    _to=wideio_typeof(1)
)
def atof(f): return float(f)


@superglue_converter(
    _from=wideio_typeof([]),
    _to=wideio_typeof((0, 1))
)
def ltot(f): return tuple(f)


@superglue_converter(
    _from=wideio_typeof((0, 1)),
    _to=wideio_typeof([])
)
def ttol(f): return list(f)


# SUPERGLUE CONVERTER SHOUD EMIT TO A MANIFEST T
# THE MANIFEST IS THEN USED TO OUTPUT THE INFO TO OUR SERVER
@superglue_converter(
    _from=wideio_typeof(numpy.ndarray((1,)),
                        drop_parameters=['shape', 'dtype'],
                        add_parameters={
        'shape': {'$and': [{'$len': 3},
                           {'$[2]': {'$lte': 3}}]
                  },
        'dtype': {'$in': map(wideio_typeref, [numpy.uint8, numpy.uint16, numpy.uint64])}
    }),
    _to=wideio_typeof(numpy.ndarray((1,)), drop_parameters=['shape', 'dtype']),
    destructive=True
)
def image_grayscale_converters(x):
    return x.mean(axis=2)


# SUPERGLUE CONVERTER SHOUD EMIT TO A MANIFEST T
# THE MANIFEST IS THEN USED TO OUTPUT THE INFO TO OUR SERVER
#  @superglue_converter(
#                       _from = wideio_typeof(numpy.ndarray((1,) ),
#                       drop_parameters=['shape','dtype'],
#                       parameters ={
#                         'shape': {'$and': [{ '$len' : 3 },
#                                           { '$[2]' : { '$lte' : 3 } } ]
#                                  },
#                         'dtype': { '$in' : map(wideio_ref, [ numpy.uint8, numpy.uint16, numpy.uint64,  ] ) }
#                       }),
#  _to   = superglue_typeof(),
#  _info:
#  destructive=True
# )
# def image_grayscale_converters(x):
#   return x.mean(axis=2)
