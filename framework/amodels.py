# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
# 
# amodels.py 
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-`           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J`   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD, http://wide.io/
# ----------------------------------------------------------------------
# CONFIDENTIAL
# ----------------------------------------------------------------------
# THIS FILE IS NOT YET DISTRIBUTED AND SHALL NOT BE REPRODUCED UNLESS YOU
# HAVE RECEIVED A WRITTEN AND SIGNED AUTHORISATION TO DO SO.
# FAILURE TO DO SO MAY EXPOSE YOU TO PROSECUTION.
# ----------------------------------------------------------------------
# THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
# KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
# PARTICULAR PURPOSE.
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################


def mirror_class_and_reparent(c, p):
    class nc(p):
        pass
    for x in dir(c):
        if (not (hasattr(nc, x))) or (x in ["__name__", "__module__"]):
            setattr(nc, x, getattr(c, x))
    return nc


def get_default(d):
    v = d.get("default", None)
    if (callable(v)):
        v = v()
    return v


def wideiomodel(c):
    """
    Instantiate the fields and provide compatibility with WIDE IO web framework.
    """
    nc = mirror_class_and_reparent(c, object)
    for f in c.FIELDS.items():
        if f[1][0] == 'TextField':
            setattr(nc, f[0], get_default(f[1][1]))
        elif f[1][0] == 'JSONField':
            setattr(nc, f[0], get_default(f[1][1]))
        elif f[1][0] == 'CharField':
            setattr(nc, f[0], get_default(f[1][1]))
        elif f[1][0] == 'ForeignKey':
            setattr(nc, f[0], get_default(f[1][1]))
        elif f[1][0] == 'IntegerField':
            setattr(nc, f[0], get_default(f[1][1]))
        elif f[1][0] == 'FloatField':
            setattr(nc, f[0], get_default(f[1][1]))
        elif f[1][0] == 'BooleanField':
            setattr(nc, f[0], get_default(f[1][1]))
        else:
            setattr(nc, f[0], get_default(f[1][1]))

    return nc


def wideio_timestamped(c):
    return c


def get_dependent_models(c):
    return []
