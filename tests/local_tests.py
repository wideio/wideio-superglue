# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
# 
# local_tests.py 
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-`           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J`   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD, http://wide.io/
# ----------------------------------------------------------------------
# CONFIDENTIAL
# ----------------------------------------------------------------------
# THIS FILE IS NOT YET DISTRIBUTED AND SHALL NOT BE REPRODUCED UNLESS YOU
# HAVE RECEIVED A WRITTEN AND SIGNED AUTHORISATION TO DO SO.
# FAILURE TO DO SO MAY EXPOSE YOU TO PROSECUTION.
# ----------------------------------------------------------------------
# THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
# KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
# PARTICULAR PURPOSE.
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
import sys
import os


os.environ["DJANGO_SETTINGS_MODULE"] = 'aiosciweb1.settings'
import aiosciweb1.settings

import numpy
from superglue.models import *
from superglue.utils import *


# BECAUSE PYTHON IS NOT SANDBOX SAFE THIS HAS TO BE CONVETED TO JAVASCRIPT
@superglue_converter(
    _from=wideio_typeof(
        numpy.ndarray((1,)),
        drop_parameters=['shape', 'dtype'],
        add_parameters={
            'shape': {'$and': {'$len': 3,
                               '$[2]': {'$lte': 3}
                               }
                      },
            'dtype': {'$in': map(wideio_typeref, map(wideio_typeof, [numpy.uint8(), numpy.uint16(), numpy.uint64()]))}
        }),
    _to=superglue_typeof(
        numpy.ndarray(
            (1,)), drop_parameters=[
            'shape', 'dtype']),
    destructive=True
)
def test(x):
    return x


###
# TEST PYTHON
###

##
# Be sure that we can express all the python types...
##

def Test1():
    for o in [1, 1, "test", u"test", 3.0, [], {}]:
        t = python2type(o)
        t.save()
        print json.dumps(t, cls=StartYnJSONEncoder)


##
# Be sure that we can express arrays
##

def Test2():
    import numpy

    for o in [
            numpy.zeros((10, 10), dtype=bool), numpy.zeros((20, 20), dtype=bool)]:
        t = python2type(o)
        t.save()
        print json.dumps(t, cls=StartYnJSONEncoder)

##
# See if we can express LLVM Types
##


def Test3():
    import llvmpy

    for o in [
            numpy.zeros((10, 10), dtype=bool), numpy.zeros((20, 20), dtype=bool)]:
        t = python2type(o)
        t.save()
        print json.dumps(t, cls=StartYnJSONEncoder)


Test1()
Test2()

##
# Enumerate converters in different files / modules
# Obviously we don't want to run any potentially harmful code at this stage (load by reflexion only)
# Or metdata
##
