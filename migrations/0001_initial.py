# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import aiosciweb1.lib.fields
import wiocore.lib.dbfields


class Migration(migrations.Migration):

    dependencies = [
        ('compute', '0001_initial'),
        ('software', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Assembly',
            fields=[
                ('name', wiocore.lib.dbfields.CharField(max_length=256, null=True, blank=True)),
                ('system', wiocore.lib.dbfields.CharField(max_length=32, null=True, blank=True)),
                ('approved', models.BooleanField(default=False)),
                ('id', wiocore.lib.dbfields.CharField(default=0, unique=True, max_length=38, primary_key=True, blank=True)),
                ('wiostate', wiocore.lib.dbfields.CharField(default=b'V', max_length=2, null=True, blank=True)),
                ('_metadata', aiosciweb1.lib.fields.JSONField(null=True, blank=True)),
                ('_nviews', models.IntegerField(default=0, db_index=True)),
                ('_nupdates', models.IntegerField(default=0, db_index=True)),
                ('created_at', models.DateTimeField(db_index=True, auto_now_add=True, null=True)),
                ('updated_at', models.DateTimeField(db_index=True, auto_now=True, null=True)),
                ('expires_at', models.DateTimeField(default=0, null=True, db_index=True)),
                ('_draft_for', models.ForeignKey(related_name='rev_draft', blank=True, to='superglue.Assembly', null=True)),
                ('package', models.ForeignKey(to='software.Package')),
                ('package_validation_job', models.ForeignKey(related_name='assemblies_being_validated', blank=True, to='compute.Job', null=True)),
                ('register_converters_job', models.ForeignKey(related_name='assemblies_registering_converters', blank=True, to='compute.Job', null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Converter',
            fields=[
                ('name', wiocore.lib.dbfields.TextField(max_length=512, null=True, blank=True)),
                ('closure_level', models.IntegerField(default=0)),
                ('to_type_fct', wiocore.lib.dbfields.TextField(max_length=512, null=True, blank=True)),
                ('id', wiocore.lib.dbfields.CharField(default=0, unique=True, max_length=38, primary_key=True, blank=True)),
                ('wiostate', wiocore.lib.dbfields.CharField(default=b'V', max_length=2, null=True, blank=True)),
                ('_metadata', aiosciweb1.lib.fields.JSONField(null=True, blank=True)),
                ('_nviews', models.IntegerField(default=0, db_index=True)),
                ('_nupdates', models.IntegerField(default=0, db_index=True)),
                ('created_at', models.DateTimeField(db_index=True, auto_now_add=True, null=True)),
                ('updated_at', models.DateTimeField(db_index=True, auto_now=True, null=True)),
                ('expires_at', models.DateTimeField(default=0, null=True, db_index=True)),
                ('_draft_for', models.ForeignKey(related_name='rev_draft', blank=True, to='superglue.Converter', null=True)),
                ('assembly', models.ForeignKey(related_name='converters', blank=True, to='superglue.Assembly', null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='TypeInfo',
            fields=[
                ('externals', aiosciweb1.lib.fields.JSONField(max_length=32768)),
                ('hint', aiosciweb1.lib.fields.JSONField(max_length=1024)),
                ('system', wiocore.lib.dbfields.CharField(max_length=32, null=True, blank=True)),
                ('parameters', aiosciweb1.lib.fields.JSONField(max_length=32768)),
                ('name', wiocore.lib.dbfields.CharField(max_length=256, null=True, blank=True)),
                ('id', wiocore.lib.dbfields.CharField(default=0, unique=True, max_length=38, primary_key=True, blank=True)),
                ('wiostate', wiocore.lib.dbfields.CharField(default=b'V', max_length=2, null=True, blank=True)),
                ('_metadata', aiosciweb1.lib.fields.JSONField(null=True, blank=True)),
                ('_nviews', models.IntegerField(default=0, db_index=True)),
                ('_nupdates', models.IntegerField(default=0, db_index=True)),
                ('created_at', models.DateTimeField(db_index=True, auto_now_add=True, null=True)),
                ('updated_at', models.DateTimeField(db_index=True, auto_now=True, null=True)),
                ('expires_at', models.DateTimeField(default=0, null=True, db_index=True)),
                ('_draft_for', models.ForeignKey(related_name='rev_draft', blank=True, to='superglue.TypeInfo', null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='converter',
            name='from_type',
            field=models.ForeignKey(related_name='export_converters', to='superglue.TypeInfo'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='converter',
            name='to_type',
            field=models.ForeignKey(related_name='import_converters', blank=True, to='superglue.TypeInfo', null=True),
            preserve_default=True,
        ),
    ]
