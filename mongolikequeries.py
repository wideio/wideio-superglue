# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
# 
# mongolikequeries.py 
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-`           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J`   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD, http://wide.io/
# ----------------------------------------------------------------------
# CONFIDENTIAL
# ----------------------------------------------------------------------
# THIS FILE IS NOT YET DISTRIBUTED AND SHALL NOT BE REPRODUCED UNLESS YOU
# HAVE RECEIVED A WRITTEN AND SIGNED AUTHORISATION TO DO SO.
# FAILURE TO DO SO MAY EXPOSE YOU TO PROSECUTION.
# ----------------------------------------------------------------------
# THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
# KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
# PARTICULAR PURPOSE.
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
import re
from functools import reduce


def xhasattr(object, path):
    try:
        xgetattr(object, path)
    except AttributeError:
        return False
    return True


def xgetattr(object, path):
    r = reduce(
        lambda x, y: (
            x.__getitem__(y) if (
                hasattr(
                    x, "__getitem__")) else getattr(
                x, y)), path.split("."), object)
    if (callable(r)):
        r = r()
    return r


operators = {
    '$and': lambda object, arg: reduce(lambda x, y: x and match_object(object, y), arg, True),
    '$or': lambda object, arg: reduce(lambda x, y: x or match_object(object, y), arg, False),
    '$nor': lambda object, arg: reduce(lambda x, y: not(x or match_object(object, y)), arg, False),
    '$lt': lambda object, arg: (object < arg),
    '$lte': lambda object, arg: (object <= arg),
    '$gt': lambda object, arg: (object > arg),
    '$gte': lambda object, arg: (object >= arg),
    '$ne': lambda object, arg: (object != arg),
    '$in': lambda object, arg: (object in arg),
    '$all': lambda object, arg: (reduce(lambda b, k: b or (k in object), arg, False)),
    '$regex': lambda object, arg: re.match(arg, object),
    '$mod': lambda object, arg: (object % arg[1] == arg[2]),
    '$nin': lambda object, arg: not (object in arg),
    '$not': lambda object, arg: not match_object(object, arg),
    '$len': lambda object, arg: match_object(len(object), arg),
    '$exists': lambda object, arg: xhasattr(object, arg),
}


def match_object(object, rule):
    for i in rule.items():
        if (i[0][0] == '$'):
            v = operators[i[0]](object, i[1])
            if not v:
                return False
        else:
            try:
                v = xgetattr(object, i[0])
            except AttributeError:
                print i[0], " not found"
                return False
            except KeyError:
                print i[0], " not found"
                return False
            except Exception as e:
                # print object, i[0]
                raise e
            if (isinstance(v, dict)):
                if not match_object(v, i[1]):
                    return False
            else:
                if (isinstance(i[1], dict)):
                    if not match_object(v, i[1]):
                        return False
                else:
                    if (v != i[1]):
                        return False
    return True


# match_object({},{})
# match_object({},{'a':2})
# match_object({'a':2},{})
# match_object({'a':2},{'a':2.0})
# match_object({'a':2},{'a':{'$lt':1.5}})
# match_object({'a':2},{'a':{'$lt':2.5}})
# match_object({'a':2},{'a':{'$lt':1.5}})
# match_object({'a':2},{'a':{'$lt':2.5}})
# match_object({'a':'truc','b':{'c':3}},{'a':{'$regex':'t(.*)c'},'b.c':{'$lte':3}})
