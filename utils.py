# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
# 
# utils.py 
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-`           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J`   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD, http://wide.io/
# ----------------------------------------------------------------------
# CONFIDENTIAL
# ----------------------------------------------------------------------
# THIS FILE IS NOT YET DISTRIBUTED AND SHALL NOT BE REPRODUCED UNLESS YOU
# HAVE RECEIVED A WRITTEN AND SIGNED AUTHORISATION TO DO SO.
# FAILURE TO DO SO MAY EXPOSE YOU TO PROSECUTION.
# ----------------------------------------------------------------------
# THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
# KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
# PARTICULAR PURPOSE.
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
###
# UTILITIES FOR SUPERGLUE
###
import json
import hashlib
import ctypes

# FIXME: We should make a version of this file that does not rely on the
# typeinfo
from superglue.models import *

from superglue.typesystem.python2 import *


def normadumps(s):
    """normalised json encoding"""
    if (type(s) in [str, float, int]):
        return '%r' % (s,)
    elif (type(s) in [unicode]):
        return ('%r' % (s.encode('utf8'),))
    elif (type(s) in [tuple, list]):
        return '[' + ",".join(map(normadumps, s)) + ']'
    elif (type(s) in [dict]):
        return '{' + ",".join(map(lambda i: "%s:%s" % (i[0], normadumps(i[1])), sorted(
            s.items(), lambda x, y: x[0] < y[0]))) + '}'
    else:
        raise Exception("Not supported")


def stdname(n):
    if n.startswith("__builtin__"):
        return n[len("__builtin__"):]
    return n


TYPE_SYSTEMS = [
    "llvm",
    "python2",
    "python3",
    "cli",
    "jvm",
    # described data using mime and json anything bigger than a specific file
    # can go to a file
    "mimejson",
    "mime"  # mime only
]


##
# EXAMINE ASSEMBLY -> RETURN ALL THE FUNCTIONS OF AN ASSEMBLY
##
class System:

    def all_types_in_module(path_to_assembly):
        pass

    def all_converters_in_module(path_to_assembly):
        pass


class CLIsystem:

    def all_types_in_module(path_to_assembly):
        pass

    def all_converters_in_module(path_to_assembly):
        pass


#@python_converter(wio_typeof(numpy.array((1,),dtype=numpy.int32)) ,
#		  wio_typeof(numpy.array((1,),dtype=numpy.uint8),lambda i : 4,True)              # < COST, DESTRUCTIVE ?
# def converter(i):
#  return i.astype(numpy.uint8)

#@python_converter(wio_typeof(numpy.array((1,),dtype=numpy.int32)) ,
#		  wio_typeof(numpy.array((1,),dtype=numpy.uint8),lambda i : 4,True)              # < COST, DESTRUCTIVE ?
# def converter(i):
#  return i.astype(numpy.uint8)


###
# VALIDATE TYPES
###


# moved in models.py


###
# EXPRESS TYPES
###


def superglue_converter(
        _from, _to, cost=1, destructive=False, *args, **kwargs):
    """
    This decorator must be used to declare that a function may be a conveter.
    """
    def dec(f):
        converters = []
        try:
            converters = json.loads(file("wideio_converters.json", "r").read())
        except Exception as e:
            print "Warning", e
            pass

        NC = {'system': 'python2',
              'module': f.__module__,
              'name': f.__name__,
              'from': _from,
              'to': _to,
              'cost': cost,
              'destructive': destructive
              }
        if len(filter(lambda f: f["from"] == wideio_typeref(
                _from) and f["to"] == wideio_typeref(_to), converters)) == 0:
            converters.append(NC)
        file("wideio_converters.json", "w").write(json.dumps(converters))
        return f
    return dec


def invoke_type(t):
    r = TypeInfo.objects.filter(**t)
    if r.count():
        return r[0]
    else:
        r = TypeInfo(**t)
        r.save()
        return r


def read_and_register_converter(filename):
    l = json.loads(file(filename).read())

    for r in l:
        tfrom = r["from"]
        tto = r["to"]
        # print wideio_typeref(tfrom)
        # print wideio_typeref(tto)
        Tfrom = invoke_type(tfrom)
        Tto = invoke_type(tto)
        # print Tfrom.id
        # print Tto.id
        t = {
            "assembly": None,
            "name": r["name"],
            "from_type": Tfrom,
            "to_type_id": Tto.id,
            "to_type_fct": "",
            "closure_level": 0,
            # r["cost"] cost ???
        }
        sc = SuperglueConverter(**t)

        sc.save()


def wideio_typeref(x):
    # FIXME: json is not normalising enough we need stronger normalisation !
    if (isinstance(x, type)):
        x = wideio_type(x)
    return {'$typeref0': x['system'] + x['name'] +
            "-" + hashlib.md5(normadumps(x)).hexdigest()}


# BECAUSE PYTHON IS NOT SANDBOX SAFE THIS HAS TO BE CONVETED TO JAVASCRIPT
#@superglue_converter(
#  _from = wideio_typeof([]),
#                       drop_parameters=['element_type'],
#                       parameters :{
#                         'element_type': {'$meta_attributes': { 'convertible' : 'mimejson'}
#                       }
#  ),
#  _to   = wideio_type("mime","image/png"),
#  destructive=True
#)


# Assembly.ReflectionOnlyLoad(
# public void PrintTypes (string fileName)
#{
#    ModuleDefinition module = ModuleDefinition.ReadModule (fileName);
#    foreach (TypeDefinition type in module.Types) {
#        if (!type.IsPublic)
#            continue;
#
#        Console.WriteLine (type.FullName);
#    }
#}


def reset_transitive_closure(self, *args, **kwargs):
    SuperglueConverter.objects.filter(level__neq=0).delete()
    do_closure(*args, **kwargs)


def do_transitive_closure(self, max_rec=10):
    na = True
    while na and (maxrec > 0):
        na = False
        new_converters = []
        for it in TypeInfo.objects.all():
            for oc in it.outconverters.objects.all():
                tt = oc.get_output_type(it)
                for sc in tt.outconverters.objects.all():
                    rt = sc.get_output_type(it)
                    cc = combined_converter(oc, sc)
                    if not find_similar_converter(cc):
                        new_converters.add(cc)
        batch_add_new_converters(new_converters)
        max_rec -= 1
