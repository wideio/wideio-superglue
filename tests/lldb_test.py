# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
# 
# lldb_test.py 
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-`           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J`   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD, http://wide.io/
# ----------------------------------------------------------------------
# CONFIDENTIAL
# ----------------------------------------------------------------------
# THIS FILE IS NOT YET DISTRIBUTED AND SHALL NOT BE REPRODUCED UNLESS YOU
# HAVE RECEIVED A WRITTEN AND SIGNED AUTHORISATION TO DO SO.
# FAILURE TO DO SO MAY EXPOSE YOU TO PROSECUTION.
# ----------------------------------------------------------------------
# THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
# KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
# PARTICULAR PURPOSE.
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
import lldb
import sys
import os
import time


def disassemble_instructions(insts):
    for i in insts:
        print i

# Set the path to the executable to debug
exe = sys.argv[1]

# Create a new debugger instance
debugger = lldb.SBDebugger.Create()

# When we step or continue, don't return from the function until the process
# stops. Otherwise we would have to handle the process events ourselves which, while doable is
# a little tricky.  We do this by setting the async mode to false.
debugger.SetAsync(False)
# debugger.HandleCommand()

# Create a target from a file and arch
print "Creating a target for '%s'" % exe

# target=debugger.CreateTarget(exe)
target = debugger.CreateTargetWithFileAndArch(exe, lldb.LLDB_ARCH_DEFAULT)
debugger.SetSelectedTarget(target)
#debugger.HandleCommand("file "+exe)
# lldb.SetSelectedTarget(target)
#process = target.LaunchSimple (None, None, os.getcwd())

# ensure target.IsValid()
print target.IsValid()

if not target:
    print "NO TARGET"
    sys.exit(-1)

# lldb.target=target

# print dir(target)
# target.LoadCore(exe)
# print dir( target.FindTypes(""))#.GetCount()
# print dir(target.modules[0])
# print target.modules[0].ResolveFileAddress()
# print dir(target.modules[0].compile_units[0])

"""
for s in target.modules[0].symbols:
  print s
  print s.name
  if target.FindFunctions(str(s.name)).GetSize():
    f=target.FindFunctions(str(s.name))[0]
    print dir(f),f.IsValid()
    F=f.GetFunction()
    print dir(F)
    print F.GetStartAddress()
    print F.GetEndAddress()
    print F.IsValid()
    insts = F.GetInstructions(target)
    print "T:"+str(F.GetType())
    disassemble_instructions(insts)
    print "---------------------"
"""

stream = lldb.SBStream()
for m in target.module_iter():
    print "module:", m
    for s in m.symbols:
        print s, s.type
    for s in m.symbols:
        if s.type == 2:
            print s
            if (not s.IsExternal()):
                print "symbol:", s.GetName()
                # if IsCodeType(s):
                print s.GetName()
                print "start address:", s.GetStartAddress()
                print "end address:", s.GetEndAddress()
                s.GetDescription(stream)
                print "symbol description:", stream.GetData()
                print "T:" + str(s.GetType())
                for i in s.instructions:
                    print "I:", i
                print s.type
                print s.synthetic
                # print dir(target),dir(s)
                print s.mangled
                print s.GetName()
                # print dir(m)
                # GetFunction().GetType()
                print "T2", m.FindFunctions(str(s.GetName()))[0].IsValid()
                print "T2", type(m.FindFunctions(str(s.GetName()))[0])
                print dir(m.FindFunctions(str(s.GetName()))[0])
                print "f", m.FindFunctions(str(s.GetName()))[0].function
                print "le", m.FindFunctions(str(s.GetName()))[0].line_entry
                print "m", m.FindFunctions(str(s.GetName()))[0].module
                print "b", m.FindFunctions(str(s.GetName()))[0].block
                print "cu", m.FindFunctions(str(s.GetName()))[0].compile_unit
                f = m.FindFunctions(str(s.GetName()))[0]
                f.compile_unit = m.compile_units[0]
                print f.function
                print dir(m)
                print m.compile_units[0]
                ##
                # Aka getImages.FindFunctions
                ##

                # print
                # "T3",target.FindFunctions(str(s.name))[0].GetFunction().GetType()
                print "/"
                # for t in m.FindTypes(s.GetType()):
                #   print t
                # for t in m.FindTypes(s.GetType()):
                #   print t


for t in target.FindSymbols("main"):
    print t

lldb.target = target
print lldb.target
# print debugger.target

process = target.LaunchSimple(None, None, os.getcwd())
# If the target is valid set a breakpoint at main
#main_bp = target.BreakpointCreateByName ("main", target.GetExecutable().GetFilename());

print main_bp

# Launch the process. Since we specified synchronous mode, we won't return
# from this function until we hit the breakpoint at main
process = target.LaunchSimple(None, None, os.getcwd())
# time.sleep(1)
# Make sure the launch went ok
if process:
    print "NO PROCESS"

if True:
        # Print some simple process info
    state = process.GetState()
    print "PROCESS", process
    if state == lldb.eStateStopped:
        # Get the first thread
        thread = process.GetThreadAtIndex(0)
        if thread:
            # Print some simple thread info
            print "THREAD", thread
            # Get the first frame
            frame = thread.GetFrameAtIndex(0)
            if frame:
                # Print some simple frame info
                print frame
                function = frame.GetFunction()
                # See if we have debug info (a function)
                if function:
                    # We do have a function, print some info for the function
                    print function
                    # Now get all instructions for this function and print them
                    insts = function.GetInstructions(target)
                    disassemble_instructions(insts)
                else:
                    # See if we have a symbol in the symbol table for where we
                    # stopped
                    symbol = frame.GetSymbol()
                    if symbol:
                        # We do have a symbol, print some i
                        print symbol
