# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
# 
# python2.py 
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-`           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J`   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD, http://wide.io/
# ----------------------------------------------------------------------
# CONFIDENTIAL
# ----------------------------------------------------------------------
# THIS FILE IS NOT YET DISTRIBUTED AND SHALL NOT BE REPRODUCED UNLESS YOU
# HAVE RECEIVED A WRITTEN AND SIGNED AUTHORISATION TO DO SO.
# FAILURE TO DO SO MAY EXPOSE YOU TO PROSECUTION.
# ----------------------------------------------------------------------
# THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
# KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
# PARTICULAR PURPOSE.
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
import os
import json

INTRISIC_ATTRIBUTE_F = {
    ("python2", "ndarray"): lambda o: {'version': o.__array_interface__["version"], 'shape': o.__array_interface__["shape"], 'descr': o.__array_interface__["descr"]},
    # wrapper types...
    ("python2", "CFunctionType"): lambda o: {'argtypes': o.argtypes, 'restype': o.restype},
}

EXTERNAL_ATTRIBUTE_F = {
}

#from lib.serializers.json import StartYnJSONEncoder


def stdname(n):
    return n


def _python2type(
        o, drop_parameters=[], drop_externals=[], add_parameters=[], add_externals=[]):
    t = TypeInfo()
    t.system = "python2"
    t.name = stdname(type(o).__name__)
    parameters = dict(
        filter(
            lambda x: x[0] not in drop_parameters,
            INTRISIC_ATTRIBUTE_F.get(
                (t.system,
                 t.name),
                lambda o: {})(o).items()))
    externals = dict(
        filter(
            lambda x: x[0] not in drop_externals,
            EXTERNAL_ATTRIBUTE_F.get(
                (t.system,
                 t.name),
                lambda o: {})(o).items()))
    parameters.update(add_parameters)
    externals.update(add_externals)
    t.parameters = json.dumps(parameters)
    t.externals = json.dumps(externals)
    return t


def python2type(
        o, drop_parameters=[], drop_externals=[], add_parameters=[], add_externals=[]):
    t = {}
    t['system'] = "python2"
    t['name'] = stdname(type(o).__module__ + "." + type(o).__name__)
    parameters = dict(
        filter(
            lambda x: x[0] not in drop_parameters,
            INTRISIC_ATTRIBUTE_F.get(
                (t['system'],
                 t['name']),
                lambda o: {})(o).items()))
    externals = dict(
        filter(
            lambda x: x[0] not in drop_externals,
            EXTERNAL_ATTRIBUTE_F.get(
                (t['system'],
                 t['name']),
                lambda o: {})(o).items()))
    parameters.update(add_parameters)
    externals.update(add_externals)
    t['parameters'] = json.dumps(parameters)
    t['externals'] = json.dumps(externals)
    return t


def python2type2(o, drop_parameters=[], drop_externals=[], add_parameters=[
], add_externals=[], try_construct=True, **kwargs):
    t = {}
    t['system'] = "python2"
    t['name'] = o.__module__ + "." + o.__name__
    parameters = {}
    if try_construct:
        try:
            parameters = dict(
                filter(
                    lambda x: x[0] not in drop_parameters,
                    INTRISIC_ATTRIBUTE_F.get(
                        (t['system'],
                         t['name']),
                        lambda o: {})(
                        o()).items()))
        except Exception as e:
            print "[superglue]exception during construction for execution parameter extraction - try specifying : try_construct = true"
            print e
    externals = {}
    if try_construct:
        try:
            externals = dict(
                filter(
                    lambda x: x[0] not in drop_externals,
                    EXTERNAL_ATTRIBUTE_F.get(
                        (t['system'],
                         t['name']),
                        lambda o: {})(
                        o()).items()))
        except Exception as e:
            print "[superglue]exception during construction for execution parameter extraction - try specifying : try_construct = true"
            print e
    parameters.update(add_parameters)
    externals.update(add_externals)
    externals.update(kwargs)

    t['parameters'] = json.dumps(parameters)
    t['externals'] = json.dumps(externals)
    return t


modT = type(os)  # type(__builtin__)
funT = type((lambda x: x))
ifunT = type(__builtins__["dir"])
codeT = type((lambda x: x).func_code)


class CodeMigration:

    @staticmethod
    def fun2obj(f):
        dict_args = {}
        args = [
            'argcount',
            'code',
            'filename',
            'flags',
            'lnotab',
            'names',
            'stacksize',
            'cellvars',
            'consts',
            'firstlineno',
            'freevars',
            'name',
            'nlocals',
            'varnames']
        for a in args:
            dict_args[a] = getattr(f.func_code, "co_" + a)
        return type(f).__name__, dict_args

    ANY2OBJ_REC = {
        funT.__name__: lambda f, ctx, g: CodeMigration.fun2obj_rec(f, ctx, g),
        ifunT.__name__: lambda f, ctx, g: (type(f).__name__, f.__name__),
        modT.__name__: lambda f, ctx, g: (type(f).__name__, f.__name__)
    }

    @staticmethod
    def any2obj_rec(n, f, ctx=None, g=None):
        if type(f).__name__ in CodeMigration.ANY2OBJ_REC:
            CodeMigration.ANY2OBJ_REC[type(f).__name__](f, ctx, g)
        else:
            ctx[n] = (type(f).__name__, f)

    @staticmethod
    def fun2obj_rec(f, ctx=None, g=None):
        if (g is None):
            g = f.func_globals
            # g.update(locals())
        if (ctx is None):
            ctx = {}
        o = CodeMigration.fun2obj(f)
        # print "o:",o
        ctx[o[1]["name"]] = o
        for i in o[1]["names"]:
            if (i not in ctx):
                if (i in g):
                    CodeMigration.any2obj_rec(i, g[i], ctx, g)
                else:
                    print "unresolved", i
        return ctx, o[1]["name"]

    @staticmethod
    def obj2fun(d, g=None):
        r = codeT(
            d['argcount'],
            d['nlocals'],
            d['stacksize'],
            d['flags'],
            d['code'],
            d['consts'],
            d['names'],
            d['varnames'],
            d['filename'],
            d['name'],
            d['firstlineno'],
            d['lnotab'],
            d['freevars'],
            d['cellvars'])
        if (g is None):
            g = globals()
        return funT(r, g)


# TODO: REFACTOR DECIDE ON NAME
wideio_type = python2type2
wideio_typeof = python2type
superglue_typeof = python2type
