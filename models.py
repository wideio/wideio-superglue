#!/usr/bin/env python
# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
# 
# models.py 
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-`           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J`   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD, http://wide.io/
# ----------------------------------------------------------------------
# CONFIDENTIAL
# ----------------------------------------------------------------------
# THIS FILE IS NOT YET DISTRIBUTED AND SHALL NOT BE REPRODUCED UNLESS YOU
# HAVE RECEIVED A WRITTEN AND SIGNED AUTHORISATION TO DO SO.
# FAILURE TO DO SO MAY EXPOSE YOU TO PROSECUTION.
# ----------------------------------------------------------------------
# THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
# KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
# PARTICULAR PURPOSE.
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
# -*- encoding:utf-8 -*-
import os
import datetime
import uuid

# try:
#import PyV8
# except:
# print "PyV8 is required for superglue"


try:
  # THIS IS BEING USED ON THE WEBSERVER
    import aiosciweb1.settings

    from django.contrib.sessions.backends.db import SessionStore
    from django.contrib.auth.models import AbstractUser
    from django.core.urlresolvers import reverse
    from django.db import models
    from djangotoolbox.fields import ListField, EmbeddedModelField
    DJANGO = 1
    #from boto.s3.connection import S3Connection
    #from boto.sqs.connection import SQSConnection
    #from boto.sqs.message import Message
    #import storages.backends.s3boto
except Exception as e:
    # print "========", e
    #import sys
    # sys.stdin.readline()
    ##
    # THIS IS BEING USED ON THE STAND ALONE VERSION
    ##
    from superglue.framework.amodels import *

    class Dec(object):

        def __getattr__(self, x):
            return None
    dec = Dec()
    DJANGO = 0


if DJANGO:
    from aiosciweb1.lib import decorators as dec
    from aiosciweb1.lib.amodels import *
    ##from wioregistration.models import ScientificTeam, User


#import pymongo

MODELS = []


# Example of external attribute
# license: OD
# GPS: xxxx

@wideio_timestamped
@wideiomodel
class TypeInfo:
    FIELDS = dict(
        system=(
            'CharField',
            dict(
                max_length=32,
                db_blank=False,
                app_blank=False)),
        name=(
            'CharField',
            dict(
                max_length=256,
                db_blank=False,
                app_blank=False)),
        parameters=('JSONField', dict(max_length=32768)),
        #< attributes related to the representation of the data
        externals=('JSONField', dict(max_length=32768)),
        #< additional attributes that help to know what we are talking about
        # < used to be sure that we retrieve the closest version
        hint=('JSONField', dict(max_length=1024))
    )

    def get_uuid(self):
        return self.uuid

    def is_larger_or_equal_than(self, at):
        if (self.system != at.system):
            return False
        if (self.name != at.name):
            return False
        if not (x_larger_or_equal(self.parameters, at.parameters)):
            return False
        if not (x_larger_or_equal(self.externals, at.externals)):
            return False
        return True

    # def __unicode__(self):
        # try:
        # return  "%s@%s"%(self.name,self.system)
        # except:
        # return "erroneous entry"
        # system = models.CharField(max_length=32)         #< underlyng type system and conversion system
        # name = models.CharField(max_length=256)          #< name of the type in current type system
        # parameters = models.TextField()         #< attributes related to the representation of the data
        # externals = models.TextField()         #< additional attributes that help to know what we are talking about
        # hint = models.CharField(max_length=256) #< used to be sure that we
        # retrieve the closest version

    def normadump(self):
        from utils import normadump as nd
        return nd(self.__dict__)

    def as_dict(self):
        return {
            "system": self.system,
            "name": self.name,
            "parameters": json.loads(self.parameters),
            "externals": json.loads(self.externals),
            "hint": json.loads(self.hint),
        }

    def __str__(self):
        try:
            return "(%s@%s:%s)" % (self.name, self.system, self.uuid)
        except:
            return "**bug**"

#    @staticmethod
#    def __unicode__(self):
#
#        return unicode(str(self))

    @staticmethod
    def can_list(request):
        return True

    # def can_view(self, request):
    #    return True

    class WIDEIO_Meta:
        NAME_USE_UNDERSCORE = True
        permissions = dec.perm_for_admin_only


MODELS.append(TypeInfo)
MODELS += get_dependent_models(TypeInfo)


@wideio_timestamped
@wideiomodel
class Assembly:

    """
    It is basically a code module that contain the definition
    of converter
    """

    FIELDS = dict(
        system=(
            'CharField',
            dict(
                max_length=32,
                db_blank=False,
                app_blank=False)),
        # < underlyng type system and conversion system
        package=('ForeignKey', dict(to='software.Package')),
        name=(
            'CharField',
            dict(
                max_length=256,
                db_blank=False,
                app_blank=False)),
        approved=('BooleanField', dict(default=False)),
        ##
        register_converters_job=('ForeignKey',
                                 {'to': 'compute.Job',
                                  'null': True,
                                  'blank': True,
                                  'related_name': 'assemblies_registering_converters'}),
        package_validation_job=('ForeignKey',
                                {'to': 'compute.Job',
                                 'null': True,
                                 'blank': True,
                                 'related_name': 'assemblies_being_validated'})
    )

    def __unicode__(self):
        return self.name

    @staticmethod
    def can_list(request):
        return True

    def can_view(self, request):
        return True

    def on_add(self, request):
        session = SessionStore()
        session.save()
        sessionid = session.session_key
        print "*********************************************************"
        print "SessionID : " + sessionid
        print "*********************************************************"
        dconv = self.do_register_converters(status="HOLD", request=request, privileged=True,
                                            on_complete={'delete_session': sessionid}, session=sessionid)
        deconv = self.do_enumerate_converters(status="HOLD", request=request, on_complete={"start_job": dconv},
                                              session=sessionid)
        dv = self.package.do_validate_package(
            request=request,
            on_complete={
                "start_job": di
                }
            )

        print str(dv), dir(dv)
        self.register_converters_job_id = str(dc)
        self.packagebranch.validation_job_id = str(dv)

        self.packagebranch.save()

    def do_register_converters(self, request, **kwargs):
        from aiosciweb1.compute.lib import create_job

        result = create_job(
            request,
            "wideio_register_converters",
            self.package_id,
            **kwargs)
        return result

    def do_enumerate_converters(self, request, **kwargs):
        from aiosciweb1.compute.lib import create_job

        result = create_job(
            request,
            "wideio_enumerate_converters",
            self.package_id,
            **kwargs)
        return result

    class WIDEIO_Meta:
        permissions = dec.perm_for_admin_only


MODELS.append(Assembly)
MODELS += get_dependent_models(Assembly)

##
# core converters
##


@wideio_timestamped
@wideiomodel
class Converter:
    FIELDS = dict(
        assembly=('ForeignKey', {'to': 'superglue.Assembly',
                                 'related_name': 'converters',
                                 'blank': True,
                                 'null': True
                                 }),
        name=('TextField', {'app_blank': False}),
        # types may be generic in which case to type is a function of from_type
        # ?
        from_type=('ForeignKey',
                   {'to': 'superglue.TypeInfo',
                    'related_name': 'export_converters',
                    'blank': False,
                    'null': False}),
        to_type=('ForeignKey',
                 {'to': 'superglue.TypeInfo',
                  'related_name': 'import_converters',
                  'blank': True,
                  'null': True}),
        # <
        to_type_fct=(
            'TextField',
            {}),
        # < if not null a javascript function that explains how to compute the result
        closure_level=(
            'IntegerField', {
                'default': 0})  # < how much iterative constructions
    )

#@wideio_timestamped
#@wideiomodel
# class SuperglueConverter:
    #assembly = models.ForeignKey(SuperglueAssembly, related_name='converters',blank=True,null=True)
    # name=models.TextField()
    # types may be generic in which case to type is a function of from_type ?
    #from_type = models.ForeignKey(TypeInfo, related_name='export_converters')
    #to_type = models.ForeignKey(TypeInfo, related_name='import_converters',blank=True,null=True)
    # to_type_fct = models.TextField() # if not null a javascript function that explains how to compute the result
    # closure_level= models.IntegerField(default=0) ## < how much iterative
    # constructions

    def __unicode__(self):
        return unicode(self.from_type) + u" -> " + unicode(self.to_type)

    @staticmethod
    def can_list(request):
        return True

    def can_view(self, request):
        return True

    def is_applicable_to(self, typeinfo):
        return self.from_type.is_larger_than(typeinfo)

    def get_converter_byte_code():
        """
        Do code migration to transmit the converter to the receiving party.
        """
        pass

    def cost_on(typeinfo):
        return -1

    class WIDEIO_Meta:
        permissions = dec.perm_for_admin_only


MODELS.append(Converter)
MODELS += get_dependent_models(Converter)
